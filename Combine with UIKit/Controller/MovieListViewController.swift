//
//  MovieListViewController.swift
//  Combine with UIKit
//
//  Created by Vilar da Camara Neto on 07/10/20.
//

import UIKit


class MovieListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
    }

}


extension MovieListViewController : UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MovieStore.allMovies.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let movie = MovieStore.allMovies[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "movie", for: indexPath) as! MovieInfoTableViewCell

        cell.fill(movie: movie)
        return cell
    }

}
