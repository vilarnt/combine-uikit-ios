//
//  MovieInfoTableViewCell.swift
//  Combine with UIKit
//
//  Created by Vilar da Camara Neto on 07/10/20.
//

import UIKit


class MovieInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var genresLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var movieImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func fill(movie: Movie) {
        self.titleLabel.text = movie.title
        self.genresLabel.text = movie.genres.sorted().joined(separator: ", ")
        self.yearLabel.text = "\(movie.year)"
    }

}
