//
//  MovieStore+Publisher.swift
//  Combine with UIKit
//
//  Created by Vilar da Camara Neto on 19/10/20.
//

import Foundation
import Combine


extension MovieStore : Publisher {

    typealias Output = [Movie]
    typealias Failure = Never

    func receive<S>(subscriber: S) where S : Subscriber, MovieStore.Failure == S.Failure, MovieStore.Output == S.Input {
        Just(Self.allMovies).receive(subscriber: subscriber)
    }

}
