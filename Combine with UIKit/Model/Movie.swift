//
//  Movie.swift
//  Combine with UIKit
//
//  Created by Vilar da Camara Neto on 07/10/20.
//

import Foundation
import Combine


class Movie {

    let imdbId: String
    let imdbUrl: URL
    let title: String
    let year: Int
    let runtimeMinutes: Int
    let genres: [String]
    let imdbRating: Float
    let certificate: String?
    let metascore: Int?
    let imageUrl: URL

    init(imdbId: String, imdbUrl: URL, title: String, year: Int, runtimeMinutes: Int, genres: [String], imdbRating: Float, certificate: String?, metascore: Int?, imageUrl: URL) {
        self.imdbId = imdbId
        self.imdbUrl = imdbUrl
        self.title = title
        self.year = year
        self.runtimeMinutes = runtimeMinutes
        self.genres = genres
        self.imdbRating = imdbRating
        self.certificate = certificate
        self.metascore = metascore
        self.imageUrl = imageUrl
    }

    lazy private var matchingTerms: [String] = {
        let terms = title.components(separatedBy: .whitespaces).map { $0.lowercased().folding(options: .diacriticInsensitive, locale: .current) }
        return Array(Set(terms))
    }()

    func matchesSearchTerm(_ term: String) -> Bool {
        let term = term.lowercased().folding(options: .diacriticInsensitive, locale: .current)
        return matchingTerms.contains { $0.starts(with: term) }
    }

    func matchesAllSearchTerms<C: Collection>(_ terms: C) -> Bool where C.Element == String {
        if terms.isEmpty {
            return true
        }
        return terms.allSatisfy { self.matchesSearchTerm($0) }
    }

}


extension Movie : Equatable {
    // Equality only cares about the IMDb identifier
    static func == (lhs: Movie, rhs: Movie) -> Bool {
        return lhs.imdbId == rhs.imdbId
    }
}


extension Movie : Hashable {
    // Hashing only cares about the IMDb identifier
    func hash(into hasher: inout Hasher) {
        self.imdbId.hash(into: &hasher)
    }
}
